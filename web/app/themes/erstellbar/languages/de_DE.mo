��          �   %   �      p     q     �     �     �     �     �     �     �  %   �  '        @     O  W   V     �     �     �     �     �     �  P   �     L  F   Y  
   �     �     �  \   �  $   +  �  P          '     /     H     P     j  
   �     �  *   �  2   �     �     	  F   	     b	     z	     �	     �	     �	     �	  I   �	     
  T   
     p
     }
     �
  �   �
  (   8                                              
                                	                                             % Responses to &laquo; &larr; Older posts &raquo; <a href="%s">Customize</a> <a href="%s">Menus</a> Comments Continue reading... Drag widgets to this footer container Drag widgets to this sidebar container. Footer widgets Images It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Newer posts &rarr; No Responses to Nothing Found One Response to Page Pages: Please assign a menu to the primary menu location under %1$s or %2$s the design. Posted on %s Ready to publish your first post? <a href="%1$s">Get started here</a>. Respond to Search Results for Sidebar widgets Sorry, but nothing matched your search terms. Please try again with some different keywords. Your comment is awaiting moderation. Project-Id-Version: FoundationPress v5.4
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-02-29 14:39+0100
PO-Revision-Date: 2016-02-29 15:06+0100
Last-Translator: 
Language-Team: 
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.8.4
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Textdomain-Support: yes
X-Poedit-Basepath: ..
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: node_modules
X-Poedit-SearchPathExcluded-1: library/erstellbar-modules/node_modules
 % Antworten zu &laquo; &larr; ältere Beiträge &raquo; <a href="%s">Anpassen</a> <a href="%s">Menüs</a> Kommentare Lesen Sie weiter... Ziehen Sie Widgets zu diesem Footerbereich Ziehen Sie Widgets zu diesem Seitenleistenbereich. Footer Widgetbereich Bilder Es scheint, wir finden nicht, was Sie suchen. Vielleicht hilft suchen. neuere Beiträge &rarr; Keine Antworten zu nichts gefunden Eine Antwort zu Seite Seiten: Bitte weisen Sie ein Menü zur primären Menüposition %1$s oder %2$s zu. Veröffentlicht am %s Bereit, den ersten Beitrag zu veröffentlichen? <a href="%1$s">Beginnen Sie hier</a> Antworten zu Suchergebnisse für Seitenleiste-Widgetbereich Es tut uns leid, aber es konnten keine Übereinstimmungen gefunden werden. Bitte versuchen Sie es erneut mit einigen anderen Suchbegriffen. Dein Kommentar wartet auf Freischaltung. 