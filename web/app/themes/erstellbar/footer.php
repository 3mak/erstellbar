<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

		</section>
		<div id="footer-container">
			<footer id="footer">
				<?php do_action( 'foundationpress_before_footer' ); ?>
				    <div class="small-12 columns">
						<?php foundationpress_footer_menu(); ?>
						<div class="icons">
							<div class="icon mail" id="mail"><a href="mailto:%6B%6F%6E%74%61%6B%74%40erstellbar.de">kontakt@erstellbar.de</a></div>
							<div class="icon phone">+49 (0) 30 911 46 428</div>
							<div class="icon geo">Kreutzerstraße 6, 16341 Panketal</div>
						</div>
						<div class="copyright">ⓒ <span id="date">2015</span> Erstellbar</div>
					</div>

				<?php do_action( 'foundationpress_after_footer' ); ?>
			</footer>
		</div>

		<?php do_action( 'foundationpress_layout_end' ); ?>

<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
		</div><!-- Close off-canvas wrapper inner -->
	</div><!-- Close off-canvas wrapper -->
</div><!-- Close off-canvas content wrapper -->
<?php endif; ?>


<?php wp_footer(); ?>
<?php do_action( 'foundationpress_before_closing_body' ); ?>
</body>
</html>
