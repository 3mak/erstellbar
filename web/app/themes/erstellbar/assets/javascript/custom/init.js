var app = (function(document, WOW,$) {

    'use strict';
    var docElem = document.documentElement,

        _userAgentInit = function() {
            docElem.setAttribute('data-useragent', navigator.userAgent);
        },
        _init = function() {
            $(document).foundation();
            _userAgentInit();
            _initEmail();
            _initWow();
            _initYear();
            //_initSmoothScroll();
            _initTopBarScroll();
            _initForm();
            document.addEventListener('touchstart', function () {}, false);

        },
        _initWow = function() {
            new WOW().init({});
        },
        _initYear = function() {
            var today = new Date(),
                dateElement = document.getElementById('date');

            dateElement.innerHTML = today.getFullYear();
        },
        _initEmail = function() {

            var elements = document.getElementsByClassName('mail');

            var pref = '&#109;a' + 'i&#108;' + '&#116;o';
            var attribut = 'hr' + 'ef' + '=';
            var first = '%6B%6F%6E%74%61%6B%74';
            var at = '%40';
            var last = '&#x65;&#x72;&#x73;&#x74;&#x65;&#x6C;&#x6C;&#x62;&#x61;&#x72;&#x2E;&#x64;&#x65;';
            var first2 = '&#x6B;&#x6F;&#x6E;&#x74;&#x61;&#x6B;&#x74;';
            var at2 = '&#x40;';
            var last2 = '&#101;&#114;&#115;&#116;&#101;&#108;&#108;&#98;&#97;&#114;&#46;&#100;&#101;';

            for (var i = 0; i < elements.length; i++) {
                elements[i].innerHTML = '<a ' + attribut + '\'' + pref + ':' + first + at + last + '\'>' + first2 + at2 + last2 + '<\/a>';
            }

        },
        _initTopBarScroll = function() {
            var offset = 10;
            var scrolled = function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop >= offset) {
                    $('body').addClass('scrolled');
                } else {
                    $('body').removeClass('scrolled');
                }
            };


            scrolled();

            $(window).scroll(function () {
                scrolled();
            });
        },
        _initForm = function() {

            $('.sign-up .show_contact').click(function(e) {
                e.preventDefault();
                $('.hide_on_form').hide();
                $('.sign-up .form').show();
            });

            $('.back').click(function(e) {
                e.preventDefault();
                $('.sign-up .form').hide();
                $('.hide_on_form').show();
            });
        };
    return {
        init: _init
    };

})(document, window.WOW, jQuery);


(function() {

    'use strict';
    app.init();

})();