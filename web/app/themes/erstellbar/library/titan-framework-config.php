<?php
/**
 * Titan Framework Config
 *
 * @package Wordpress
 * @author   Sven Friedemann <sven@ertellbar.de>
 * @licence  MIT
 */

add_action('tf_create_options', 'erstellbar_create_options');
function erstellbar_create_options() {

    $titan = TitanFramework::getInstance('erstellbar');

    /**
     * Create Meta Box for Post
     */
    $postMetabox = $titan->createMetaBox( array(
        'name' => __('Options'),
        'post_type' => 'post',
    ));

    $postMetabox->createOption(array(
        'name' => __('URL'),
        'id' => 'post-url',
        'type' => 'text',
    ));

    $referenceMetabox = $titan->createMetaBox( array(
        'name' => __('Additional Information'),
        'post_type' => 'reference',
    ));

    $referenceMetabox->createOption(array(
        'name' => 'Website',
        'id' => 'reference_url',
        'type' => 'text',
    ));

    $themeOptions = $titan->createContainer( array(
        'type' => 'customizer',
        'name' => 'Settings',
        'panel' => 'Front Page',
    ));

    $themeOptions->createOption(array(
        'name' => 'Page Slogan',
        'type' => 'text',
        'id' => 'front_page_slogan',
    ));

    $themeOptions->createOption(array(
        'name' => 'Page Name',
        'type' => 'text',
        'id' => 'front_page_name',
    ));

    $themeOptions->createOption(array(
        'name' => 'Page Text',
        'type' => 'text',
        'id' => 'front_page_text',
    ));
}