<?php
/**
 * Reference Post type
 *
 * @package  FoundationPress
 * @author   Sven Friedemann <sven@ertellbar.de>
 * @licence  MIT
 */

// Register Custom Post Type
function register_reference_post_type() {

    $labels = array(
        'name'                  => _x( 'References', 'Post Type General Name', 'erstellbar' ),
        'singular_name'         => _x( 'Reference', 'Post Type Singular Name', 'erstellbar' ),
        'menu_name'             => __( 'Reference', 'erstellbar' ),
        'name_admin_bar'        => __( 'References', 'erstellbar' ),
        'archives'              => __( 'Reference Archive', 'erstellbar' ),
        'parent_item_colon'     => __( 'Parent Reference:', 'erstellbar' ),
        'all_items'             => __( 'All References', 'erstellbar' ),
        'add_new_item'          => __( 'Add New Reference', 'erstellbar' ),
        'add_new'               => __( 'Add New', 'erstellbar' ),
        'new_item'              => __( 'New Reference', 'erstellbar' ),
        'edit_item'             => __( 'Edit Reference', 'erstellbar' ),
        'update_item'           => __( 'Update Reference', 'erstellbar' ),
        'view_item'             => __( 'View Reference', 'erstellbar' ),
        'search_items'          => __( 'Search Refrence', 'erstellbar' ),
        'not_found'             => __( 'Not found', 'erstellbar' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'erstellbar' ),
        'featured_image'        => __( 'Featured Image', 'erstellbar' ),
        'set_featured_image'    => __( 'Set featured image', 'erstellbar' ),
        'remove_featured_image' => __( 'Remove featured image', 'erstellbar' ),
        'use_featured_image'    => __( 'Use as featured image', 'erstellbar' ),
        'insert_into_item'      => __( 'Insert into item', 'erstellbar' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'erstellbar' ),
        'items_list'            => __( 'Reference list', 'erstellbar' ),
        'items_list_navigation' => __( 'Reference list navigation', 'erstellbar' ),
        'filter_items_list'     => __( 'Reference items list', 'erstellbar' ),
    );
    $args = array(
        'label'                 => __( 'Reference', 'erstellbar' ),
        'description'           => __( 'References', 'erstellbar' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail'),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-awards',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'reference', $args );

}
add_action( 'init', 'register_reference_post_type', 0 );