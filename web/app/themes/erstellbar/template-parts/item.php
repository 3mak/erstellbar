<?php
/**
 * Template part Reference Item
 *
 * @package WordPress
 * @subpackage Erstellbar Theme
 * @author Sven Friedemann <sven@ertellbar.de>
 * @copyright Copyright (c) 2015 Erstellbar, Sven Friedemann
 */

?>
<?php
$titan = TitanFramework::getInstance('erstellbar');
?>
<div class="item">
    <figure class="effect-zoe">
        <?php the_post_thumbnail('reference-default'); ?>
        <figcaption>
            <p class="description"><?php echo get_the_content(); ?></p>
            <div class="foot">
                <h2><?php the_title(); ?></h2>
                <p class="icon-links">
                    <a href="<?php echo $titan->getOption('reference_url');?>" title="Seite ansehen" target="_blank"><span class="icon-eye"></span></a>
                </p>
            </div>
        </figcaption>
    </figure>
</div>