<?php
/**
 * Template part for top bar menu
 *
 * @package WordPress
 * @subpackage Erstellbar Theme
 * @author Sven Friedemann <sven@ertellbar.de>
 * @copyright Copyright (c) 2015 Erstellbar, Sven Friedemann
 */

?>
<?php $has_featured_image = (true === has_post_thumbnail(  $post->ID ) && false === is_single( $post->ID )); ?>
<?php if ( $has_featured_image ) : ?>
    <nav class="top" role="navigation">
        <h1 class="nocontent">Navigation</h1>
        <div class="row">
            <div class="small-12 columns">
                <a class="left-off-canvas-toggle show-for-small-only icon" data-toggle="mobile-menu" href="#">&#9776;</a>
                <div class="logo"><a href="<?php echo home_url('/') ?>"></a></div>
                <?php foundationpress_top_bar(); ?>
            </div>
        </div>
    </nav>
<?php endif; ?>

<nav class="top <?php echo ( $has_featured_image ) ? 'fixed' : 'persistent' ?>" role="navigation">
    <h1 class="nocontent">Navigation</h1>
    <div class="row">
        <div class="small-12 columns">
            <a class="left-off-canvas-toggle show-for-small-only icon" data-toggle="mobile-menu" href="#">&#9776;</a>
            <div class="logo black"><a href="<?php echo home_url('/') ?>"></a></div>
            <?php foundationpress_top_bar(); ?>
        </div>
    </div>
</nav>
