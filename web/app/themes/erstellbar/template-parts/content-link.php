<?php
/**
 * The link template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<?php $titan = TitanFramework::getInstance('erstellbar'); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class('panel'); ?>>
	<div class="row">
		<?php if (has_post_thumbnail() ) : ?>
		<div class="medium-4 columns">
			<?php the_post_thumbnail('small'); ?>
		</div>
		<?php endif; ?>
		<header class="<?php echo (has_post_thumbnail() ? 'medium-8': 'small-12');?> columns">
			<h2><a href="<?php echo $titan->getOption('post-url') ?>" title="<?php the_title(); ?>" target="_blank"><?php the_title(); ?></a></h2>
			<?php foundationpress_entry_meta(); ?>
		</header>
	</div>
	<div class="entry-content">
		<?php the_content(); ?>
	</div>
	<footer>
		<?php $tag = get_the_tags(); if ( $tag ) { ?><p><?php the_tags(); ?></p><?php } ?>
	</footer>
</article>