<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Erstellbar Theme
 * @author Sven Friedemann <sven@ertellbar.de>
 * @copyright Copyright (c) 2015 Erstellbar, Sven Friedemann
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('panel'); ?>>
	<div class="row">
		<?php if (has_post_thumbnail() ) : ?>
				<?php the_post_thumbnail('small'); ?>
		<?php endif; ?>
		<header>
			<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
			<?php foundationpress_entry_meta(); ?>
		</header>
	</div>
	<div class="entry-content">
		<?php the_content( __( 'Continue reading...', 'foundationpress' ) ); ?>
	</div>
	<footer>
		<?php $tag = get_the_tags(); if ( $tag ) { ?><p><?php the_tags(); ?></p><?php } ?>
	</footer>
</article>