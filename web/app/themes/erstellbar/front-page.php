<?php
/**
 * The Template for displaying the front page
 *
 * @package WordPress
 */

$titan = TitanFramework::getInstance( 'erstellbar' );
get_header(); ?>
    <?php /* Start loop */ ?>
    <?php while (have_posts() ) : the_post(); ?>
        <?php get_template_part( 'template-parts/featured-image' ); ?>
        <div class="header">
            <div class="row">
                <div class="large-12 columns">
                    <div class="head">
                        <hgroup>
                            <h1 class="wow fadeInDown" style="visibility: visible; animation-name: fadeInDown;"><?php echo $titan->getOption('front_page_slogan')?></h1>
                            <h1 class="wow fadeIn" data-wow-delay="1s" style="visibility: visible; animation-delay: 1s; animation-name: fadeIn;"><span><?php echo $titan->getOption('front_page_name')?></span></h1>
                        </hgroup>
                        <p><?php echo $titan->getOption('front_page_text')?></p>
                        <a href="#ihre-idee" data-scroll="" class="button radius">Anfrage senden.</a>
                    </div>
                </div>
            </div>
        </div>
        <?php the_content(); ?>
    <?php endwhile; // End the loop ?>

<?php get_footer(); ?>
