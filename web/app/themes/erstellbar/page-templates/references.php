<?php
/*
 * Template Name: References
 */

get_header(); ?>
<div id="references-page" role="main">
    <article class="main-content">
            <h2 class="page-title">
                Referenzen. <br>
                <strong>Unsere Entwicklungen.</strong>
            </h2>
        <!-- Row for main content area -->
        <div class="row collapsed">
        <?php
        $query = new WP_Query(array(
            'post_type' => array('reference'),
            'posts_per_page' => 50,
        ));
        $columns = 3;
        $publish_posts_count = $query->post_count;
        $modulus = $publish_posts_count % $columns;
        $max_posts_per_column = ($publish_posts_count - $modulus) / $columns;
        $i = 0;
        ?>
        <?php if ( $query->have_posts() ) : ?>
            <?php /* Start the Loop */ ?>
            <?php while ( $query->have_posts() ) : $query->the_post(); ?>

                <?php if ( 0 === $i ) : ?>
                    <div class="medium-4 columns">
                <?php endif; $i += 1; ?>

                <?php get_template_part('template-parts/item'); ?>
                <?php $offset = ($modulus > 0) ? 1: 0; ?>
                <?php if ( $i >= $max_posts_per_column + $offset ) : ?>
                    </div>
                    <?php $i = 0; $modulus = $modulus - 1; ?>
                <?php endif; ?>
            <?php endwhile; ?>
        <?php else : ?>
            <?php get_template_part('content', 'none'); ?>

        <?php endif; // End have_posts() check. ?>
        </div>

        <?php /* Display navigation to next/previous pages when applicable */ ?>
        <?php if ( function_exists( 'foundationpress_pagination' ) ) { foundationpress_pagination(); } else if ( is_paged() ) { ?>
            <nav id="post-nav">
                <div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
                <div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
            </nav>
        <?php } ?>
    </article>
</div>
<?php wp_reset_postdata(); ?>
<?php get_footer(); ?>
