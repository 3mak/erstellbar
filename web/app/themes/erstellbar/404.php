<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<section class="color-container error vertical-center">
<div class="center text-center">
	<div class="border">
				<h1>404</h1>
				<p>Gründe dafür könnten sein, dass Sie eine falsche oder veraltete URL aufgerufen haben - bitte überprüfen Sie diese noch einmal.
					<br/><a href="<?php echo get_home_url(); ?>">Zurück zur Startseite</a></p>
	</div>
</div>
<?php get_footer();
